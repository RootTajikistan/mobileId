<?php
/**
 * User: Bakhtiyor Ruziev
 * Date: 15.02.15
 * Time: 19:23
 */

namespace Asad\mobileId;


/**
 * Class MobileId
 * @package Asad\mobileId
 */
class MobileId {


    /**
     * @var bool
     */
    public $usePrefix = true;
    /**
     * @var array
     */
    public $mobileOperators = [
        'Babilon-M' => [
            '98',
            '918',
        ],
        'Beeline' => [
            '915',
            '917',
            '919'
        ],
        'Megafon' => [
            '90'
        ],
        'Tcell' => [
            '93',
            '92',
            '50',
        ],
        'TK-Mobile' => [
            '95'
        ]
    ];


    /**
     * @param $mobileNumber
     * @return bool
     */
    public function checkCorrectNumber($mobileNumber)
    {
        if ($this->usePrefix) {
            $pattern = "/^992\d{9}$/";
        } else {
            $pattern = "/^\d{9}$/";
        }
        return (preg_match($pattern,$mobileNumber) == 1);
    }

    /**
     * @param $mobileNumber
     * @return int|string
     * @throws \Exception
     */
    public function getMobileOperator($mobileNumber)
    {
        if (!$this->checkCorrectNumber($mobileNumber)) {
            throw new \Exception('Mobile number it\'s not correct.');
        }

        if ($this->usePrefix) {
            $pattern = "/^992(\d{3})\d{6}$/";
        } else {
            $pattern = "/^(\d{3})\d{6}$/";
        }


        preg_match($pattern, $mobileNumber, $matches);
        $code = $matches[1];
        return $this->search($code);

    }


    /**
     * @param $mobileNumber
     * @return bool
     */
    public function isValidNumber($mobileNumber)
    {
        if (!$this->checkCorrectNumber($mobileNumber)) {
            return false;
        }
        if ($this->usePrefix) {
            $pattern = "/^992(\d{3})\d{6}$/";
        } else {
            $pattern = "/^(\d{3})\d{6}$/";
        }


        preg_match($pattern, $mobileNumber, $matches);
        $code = $matches[1];
        if ($this->search($code)) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * @param $code
     * @return bool|string
     */
    private function search($code)
    {
        foreach($this->mobileOperators as $key => $value) {
            foreach($value as $c) {
                if (preg_match("/${c}/",$code) == 1)
                    return $key;
            }
        }

        return false;

    }





} 