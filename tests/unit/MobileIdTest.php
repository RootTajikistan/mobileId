<?php
namespace Asad\Test;

use Asad\mobileId\MobileId;

class MobileIdTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testCheckingCorrectMobileNumber()
    {
        $mobile = new MobileId();
        $this->assertEquals("Beeline", $mobile->getMobileOperator("992915041121"));
        $this->assertEquals("Tcell", $mobile->getMobileOperator("992935634007"));

        $mobile->usePrefix = false;
        $this->assertEquals("Beeline", $mobile->getMobileOperator("915041121"));
        $this->assertEquals("Tcell", $mobile->getMobileOperator("935634007"));

        $mobile->usePrefix = true;


        $this->assertNotEquals("Beeli", $mobile->getMobileOperator("992935634007"));
        $this->assertNotEquals("Tcell", $mobile->getMobileOperator("992917634007"));


        $this->assertTrue($mobile->isValidNumber("992915041121"));
        $this->assertFalse($mobile->isValidNumber("992913041121"));
        $this->assertFalse($mobile->isValidNumber("992912041121"));
        $this->assertFalse($mobile->isValidNumber("9929120"));

    }

}